**example**
```

const logger = require( 'logger').CreateLogger({isDebug:true, writeFile:"fileName.txt"});
logger.Log();	// 當isDebug設定為false, 則logger.Log()訊息隱藏
logger.Info();
logger.Error();
logger.Warning();
```