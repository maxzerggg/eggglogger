(()=>{
"use strict";
	const colors = require( 'colors' );
	const util = require( 'util' );
	const fs = require( 'fs' );

	module.exports = {
		CreateLogger:(option)=>{
			let { isDebug=true, writeFile=null, isNeedTimeStamp=true } = option;

			// 開啟檔案串流
			let fd = null;
			if( writeFile ){
				try{
					fd = fs.openSync(writeFile, "a");
				}catch(err){
					throw(err);
				}
			}

			// 設定顏色
			colors.setTheme({
				silly: 'rainbow',
				input: 'grey',
				verbose: 'cyan',
				prompt: 'grey',
				info: 'green',
				data: 'grey',
				help: 'cyan',
				warn: 'yellow',
				debug: 'blue',
				error: 'red'
			});

			// 提供的

			return {
				Log:(str="")=>{
					let msg = generateMsg(str);
					if( isDebug ){
						console.log(msg.prompt);
					}

					if( fd ){
						fs.appendFileSync(fd, msg+'\r\n', "utf8");
					}
				},
				Info:(str="")=>{
					let msg = generateMsg(str);
					console.log(msg.info);

					if( fd ){
						fs.appendFileSync(fd, msg+'\r\n', "utf8");
					}
				},
				Error:(str)=>{
					let msg = generateMsg(str);
					console.log(msg.error);

					if( fd ){
						fs.appendFileSync(fd, msg+'\r\n', "utf8");
					}
				},
				Warning:(str)=>{
					let msg = generateMsg(str);
					console.log(msg.warn);

					if( fd ){
						fs.appendFileSync(fd, msg+'\r\n', "utf8");
					}
				}
			};

			function generateMsg(str=""){
				let t = new Date();
				let month = t.getMonth()+1;
				month = month.toString().padStart(2, 0);
				let tStr = isNeedTimeStamp ? `[${t.getFullYear()}-${month}-${t.getDate().toString().padStart(2, 0)} ${t.getHours().toString().padStart(2, 0)}:${t.getMinutes().toString().padStart(2, 0)}:${t.getSeconds().toString().padStart(2, 0)}.${t.getMilliseconds().toString().padStart(3, 0)}]` : "";
				str = typeof str === "object" ? util.inspect(str, {showHidden: false, depth: null, colors:true}) : str;
				return tStr + str;
			}
		}
	};
})();