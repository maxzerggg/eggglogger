(()=>{
	"use strict"

	const logger = require( "./log" ).CreateLogger({isDebug:false, writeFile:"record.txt"});

	logger.Log("hi~");
	logger.Info("this is test");
	logger.Warning("becareful~");
	logger.Error("woops! something bad..");
})();